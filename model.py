import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity

class Substitution:

  def __init__(self):
    self.name = "Substitution Engine"
    self.cosine_sim = None
    self.data_frame = None

  def combined_features(self, row):
      return str(row['product_id'])+" "+str(row['product_category_name'])+" "+str(row['product_name_length'])+" "+str(row['product_description_length'])+" "+str(row['product_weight_g'])+" "+str(row['product_length_cm'])+" "+str(row['product_height_cm'])+" "+str(row['product_width_cm'])

  def get_product_id_index(self, product_id):
      return self.data_frame[self.data_frame.product_id == product_id]["index"].values[0]

  def get_product_id_id_from_index(self, index):
      return (self.data_frame[self.data_frame.index == index]["product_id"].values[0])

  def train_model(self):
    self.data_frame = pd.read_csv("./train_data.csv")
    features = ['product_category_name', 'product_name_length', 'product_description_length',
           'product_weight_g', 'product_length_cm', 'product_height_cm', 'product_width_cm']
    for feature in features:
        self.data_frame[feature] = self.data_frame[feature].fillna('')
    self.data_frame["combined_features"] = self.data_frame.apply(self.combined_features, axis =1)
    cv = CountVectorizer()
    count_matrix = cv.fit_transform(self.data_frame["combined_features"])
    self.cosine_sim = cosine_similarity(count_matrix)

  def get_substitution(self, product_id):
    if(self.data_frame is None):
      self.train_model()
    product_id_index = self.get_product_id_index(product_id)
    similar_product_ids = list(enumerate(self.cosine_sim[product_id_index]))
    sorted_similar_product_ids = sorted(similar_product_ids, key=lambda x:x[1], reverse=True)
    response = []
    for product_id in sorted_similar_product_ids:
      if len(response) >= 5:
        break
      response.append(self.get_product_id_id_from_index(product_id[0]))
    return response




