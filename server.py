import sys
sys.path.append(".")
from model import Substitution
from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route('/train', methods=['GET'])
def trainModel():
  substitution.train_model()
  return jsonify({"status": "Success", "message": "Model Successfully Trained"})

@app.route('/substitution', methods=['GET'])
def getSubstitution():
  product_id = request.args.get('product_id')
  return jsonify(results=substitution.get_substitution(product_id))

if __name__ == '__main__':
  substitution = Substitution()
  app.run(port = 5000, debug=True)